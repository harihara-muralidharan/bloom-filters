#$bf build -k <key file> -f <fpr> -n <num. distinct keys> -o <output file>
#0    1     2   3         4   5    6    7                  8   9
#$bf query -i <input file> -q <queries>
#0    1     2   3          4    5

from sys import argv
from Blocked_Bloom_Filter import Blocked_Bloom_Filter

def Write_Object(op_file, B):
	f = open(op_file, 'w')
	f.write(str(B.p)+"\n")
	f.write(str(B.n)+"\n")
	f.write(str(B.b)+"\n")
	f.write(str(B.k)+"\n")
	f.write(str(B.m)+"\n")
	f.write(str(B.num_blocks)+"\n")
	f.write(str(B.num_keys_per_block)+"\n")
	temp_str = ''
	for h in B.hash_seeds:
		temp_str = temp_str+str(h)+' '
	f.write(temp_str[:-1]+"\n")
	temp_str = ''
	for b in B.Blocked_Bloom_Filter:
		temp_str = temp_str+str(b)+' '
	f.write(temp_str[:-1]+"\n")
	f.close()

if argv[1] == 'build':
	key_file = argv[3]
	keys = open(key_file,'r').readlines()
	p = float(argv[5])
	n = int(argv[7])
	op_file = argv[9]

	B = Blocked_Bloom_Filter(n, p, 512, '')
	B.Populate_Blocked_Bloom_Filter(keys)
	Write_Object(op_file, B)

if argv[1] == 'query':
	B = Blocked_Bloom_Filter(0,0,0,argv[3])
	queries = open(argv[5],'r').readlines()
	OP = B.Query_Blocked_Bloom_Filter(queries)
	for i in range(len(queries)):
		if not(OP[i]):
			out = "Y"
		else:
			out = "N"
		print(queries[i][:-1],'\t',out)


