#$bf build -k <key file> -f <fpr> -n <num. distinct keys> -o <output file>
#0    1     2   3         4   5    6    7                  8   9
#$bf query -i <input file> -q <queries>
#0    1     2   3          4    5
from sys import argv
from BloomFilter import BloomFilter

def Write_Object(op_file, B):
	f = open(op_file, 'w')
	f.write(str(B.p)+"\n")
	f.write(str(B.n)+"\n")
	f.write(str(B.m)+"\n")
	f.write(str(B.k)+"\n")
	temp_str = ''
	for h in B.hash_seeds:
		temp_str = temp_str+str(h)+' '
	f.write(temp_str[:-1]+"\n")
	temp_str = ''
	for b in B.Bloom_Filter:
		temp_str = temp_str+str(b)+' '
	f.write(temp_str[:-1]+"\n")
	f.close()

	
if argv[1] == 'build':
	key_file = argv[3]
	keys = open(key_file,'r').readlines()
	p = float(argv[5])
	n = int(argv[7])
	op_file = argv[9]

	B = BloomFilter(n, p, '')
	B.Populate_Bloom_Filter(keys)
	Write_Object(op_file, B)

if argv[1] == 'query':
	B = BloomFilter(0,0,argv[3])
	queries = open(argv[5],'r').readlines()
	OP = B.Query_Bloom_Filter(queries)
	for i in range(len(queries)):
		if not(OP[i]):
			out = "Y"
		else:
			out = "N"
		print(queries[i][:-1],'\t',out)


