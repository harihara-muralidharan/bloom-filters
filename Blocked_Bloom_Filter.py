import numpy as np
from mmh3 import hash128
from random import sample 
from math import ceil, floor, log

class Blocked_Bloom_Filter:
	def __init__(self, n, p, b, filepath):
		if filepath == '':
			self.p = p
			self.n = n
			self.b = b
			self.k = None
			self.m = None
			self.num_blocks = None
			self.num_keys_per_block = None
			self.hash_seeds = None
			self.Blocked_Bloom_Filter = None
			self.Compute_Blocked_Bloom_Filter_Parameters()
		else:
			self.Load_From_File(filepath)
			
	def Load_From_File(self, filepath):
		lines = open(filepath,'r').readlines()
		self.p = float(lines[0])
		self.n = int(lines[1])
		self.b = int(lines[2])
		self.k = int(lines[3])
		self.m = int(lines[4])
		self.num_blocks = float(lines[5])
		self.num_keys_per_block = float(lines[6])
		self.hash_seeds = [int(seed) for seed in lines[7].split(' ')]
		self.Blocked_Bloom_Filter = [x=='True' for x in lines[8].split(' ')]

	def Compute_Blocked_Bloom_Filter_Parameters(self):
		self.num_keys_per_block = ceil(-1*self.b*log(2, np.e)**2/log(self.p, np.e))
		self.num_blocks = ceil(self.n/self.num_keys_per_block)
		self.k = ceil((self.b/self.num_keys_per_block)*log(2, np.e))
		self.m = self.num_blocks*self.b
		self.hash_seeds = sample(range(ceil(0.5*self.k), 10*self.k), self.k+1)

	def Populate_Blocked_Bloom_Filter(self, keys):
		self.Blocked_Bloom_Filter = [False]*(self.b*self.num_blocks)
		for key in keys:
			block_id = int(hash128(key,self.hash_seeds[0],signed=False)%self.m/self.b)
			block_start = block_id*self.b
			for seed in self.hash_seeds[1:]:
				hash_val = hash128(key,seed,signed=False)%self.b
				self.Blocked_Bloom_Filter[block_start+hash_val] = True

	def Query_Blocked_Bloom_Filter(self, query_seq):
		BF_OP = [False]*len(query_seq)
		for j in range(0, len(query_seq)):
			q = query_seq[j]
			block_id = int(hash128(q, self.hash_seeds[0], signed = False)%self.m/self.b)
			block_start = block_id*self.b
			for seed in self.hash_seeds[1:]:
				hash_val = hash128(q,seed,signed=False)%self.b
				if self.Blocked_Bloom_Filter[block_start+hash_val] == False:
					BF_OP[j] = True
					break
		return BF_OP