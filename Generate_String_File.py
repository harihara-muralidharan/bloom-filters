from itertools import product

def Create_String_File():
	kmers = list(product('ATGC', repeat=12))
	with open('kmers_BF.txt', 'w') as f:
		for item in kmers:
			item = ' '.join(item)
			f.write(item+"\n")
	f.close()

Create_String_File()