import time
import numpy as np
import pandas as pd
from random import sample 
from BloomFilter import BloomFilter

def Return_Samples(filepath, n, q): 
	strings = open(filepath).readlines()
	samples = sample(strings, n+q)
	
	keys = samples[:n]
	queries_1 = samples[n:]
	queries_2 = samples[int(n-q/2):int(n+q/2)]
	queries_3 = samples[n-q:n]
	return keys, queries_1, queries_2, queries_3

def Return_Experimental_Evaluations():
	key_size = [10000, 25000, 50000, 75000, 100000, 
				250000, 500000, 750000, 1000000, 5000000]
	False_Positive_Rate = [0.01, 0.05, 0.1, 0.15, 0.20, 0.25, 0.30]
	query_size = 1000
	op = []
	for n in key_size:
		for p in False_Positive_Rate:
			keys, q1, q2, q3 = Return_Samples('Kmers_BF.txt', n, query_size)

			B = BloomFilter(n, p, '')
			B.Populate_Bloom_Filter(keys)
			start = time.time()
			OP_1 = B.Query_Bloom_Filter(q1)
			done = time.time()
			q_time_1 = round(done - start, 4)

			start = time.time()
			OP_2 = B.Query_Bloom_Filter(q2)
			done = time.time()
			q_time_2 = round(done - start, 4)

			start = time.time()
			OP_3 = B.Query_Bloom_Filter(q3)
			done = time.time()
			q_time_3 = round(done - start, 4)

			d = {'n':n, 'p':p, 'FPR_1':round(1-np.sum(OP_1)/100000.0, 4), 'FPR_2':round(1-np.sum(OP_2)/50000.0, 4),
				 'Time_Query_1':q_time_1, 'Time_Query_2':q_time_2, 'Time_Query_3':q_time_3}
			op.append(d)
			print(d)
			print(B.m)
	df_op = pd.DataFrame(op)
	df_op.to_csv('Bloom_Filter_Experiments.csv')

Return_Experimental_Evaluations()