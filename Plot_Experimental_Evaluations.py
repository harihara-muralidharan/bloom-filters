#!/usr/bin/env python
# coding: utf-8
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

rcParams = {'font.size': 20 , 'font.weight': 'normal', 
            'font.family': 'sans-serif',
            'axes.unicode_minus':False, 
            'axes.labelweight':'normal'}
plt.rcParams.update(rcParams)

filepath = sys.argv[1]
save_path = 'Plots/'+sys.argv[2]
df = pd.read_csv(filepath)
del df['Unnamed: 0']
print(df.head())

colors = ['red','blue','green','cyan','orange','black','teal']
unique_p = df['p'].unique().tolist()

fig_1, ax_1 = plt.subplots(1,2, figsize = (16,10)) 
for i in range(len(unique_p)):
    p = unique_p[i]
    df_sliced = df[df['p'] == p]
    df_sliced.plot.scatter('n','FPR_1',color = colors[i], s = 100, ax = ax_1[0], rot = 90)
    ax_1[0].axhline(p, linestyle = '--', color = colors[i])
    
    df_sliced.plot.scatter('n','FPR_2',color = colors[i], s = 100, ax = ax_1[1], rot = 90)
    ax_1[1].axhline(p, linestyle = '--', color = colors[i])
ax_1[0].set_ylim([-0.01, 0.39])
ax_1[0].set_ylabel('Empirical False Positive Rate')
ax_1[0].set_xlabel('Number of Keys')
ax_1[0].set_title('Empirical FPR Vs Number of Keys: Case 1')
ax_1[1].set_ylim([-0.01, 0.39])
ax_1[1].set_ylabel('')
ax_1[1].set_xlabel('Number of Keys')
ax_1[1].set_title('Empirical FPR Vs Number of Keys: Case 2')
fig_1.tight_layout()
fig_1.savefig(save_path+'_Empirical_FPR.pdf')

fig_2, ax_2 = plt.subplots(figsize = (18,10))
df.plot.scatter('p','Time_Query_1',c = 'n', cmap = 'jet', s = 80, marker = '$1$', ax = ax_2, colorbar = False)
df.plot.scatter('p','Time_Query_2',c = 'n', cmap = 'jet', s = 80, marker = '$2$', ax = ax_2, colorbar = False)
df.plot.scatter('p','Time_Query_3',c = 'n', cmap = 'jet', s = 80, marker = '$3$', ax = ax_2, colorbar = True)
ax_2.set_ylim([0.001, 0.007])
ax_2.set_xlabel('False Positive Rate')
ax_2.set_ylabel('Time in Seconds')
fig_2.tight_layout()
fig_2.savefig(save_path+'_Query_Time.pdf')
plt.show()