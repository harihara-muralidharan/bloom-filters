import numpy as np
from mmh3 import hash128
from random import sample 
from math import ceil, floor, log

class BloomFilter:
	def __init__(self, n, p, filepath):
		if filepath  == '':
			self.p = p
			self.n = n
			self.m = None
			self.k = None
			self.hash_seeds = None
			self.Bloom_Filter = None
			self.Return_Bloom_Filter_Parameters()
		else:
			self.Load_From_File(filepath)

	def Load_From_File(self, filepath):
		lines = open(filepath,'r').readlines()
		self.p = float(lines[0])
		self.n = int(lines[1])
		self.m = int(lines[2])
		self.k = int(lines[3])
		self.hash_seeds = [int(seed) for seed in lines[4].split(' ')]
		self.Bloom_Filter = [x=='True' for x in lines[5].split(' ')]

	def Return_Bloom_Filter_Parameters(self):
		self.m = ceil(-1*self.n*log(self.p, np.e)/(log(2, np.e)**2))
		self.k = ceil((self.m/self.n)*log(2, np.e))
		self.hash_seeds = sample(range(floor(0.5*self.k), 10*self.k), self.k)

	def Populate_Bloom_Filter(self, keys):
		self.Bloom_Filter = [False]*self.m
		for key in keys:
			for seed in self.hash_seeds:
				hash_val = hash128(key,seed,signed=False)%self.m
				self.Bloom_Filter[hash_val] = True

	def Query_Bloom_Filter(self, query_seq):
		BF_OP = [False]*len(query_seq)
		for j in range(0, len(query_seq)):
			q = query_seq[j]
			for seed in self.hash_seeds:
				hash_val = hash128(q,seed,signed=False)%self.m
				if self.Bloom_Filter[hash_val] == False:
					BF_OP[j] = True
					break
		return BF_OP