from random import sample 

def Return_Samples(filepath, n, q): 
	strings = open(filepath).readlines()
	samples = sample(strings, n+q)
	
	keys = samples[:n]
	queries_1 = samples[n:]
	queries_2 = samples[int(n-q/2):int(n+q/2)]
	queries_3 = samples[n-q:n]
	return keys, queries_1, queries_2, queries_3

keys, q1, q2, q3 = Return_Samples('Kmers_BF.txt', 10000, 1000)
f = open("Keys.txt","w")
f.writelines(keys)
f.close()

f = open("Queries.txt","w")
f.writelines(q2)
f.close()